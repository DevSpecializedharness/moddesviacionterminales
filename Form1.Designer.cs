﻿
namespace ModDesviacionTerminales
{
    partial class frmDesviacionTerminales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RdbTerminal = new System.Windows.Forms.RadioButton();
            this.RdbCable = new System.Windows.Forms.RadioButton();
            this.btnWips = new System.Windows.Forms.Button();
            this.TxtWip = new System.Windows.Forms.TextBox();
            this.lblWip = new System.Windows.Forms.Label();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.DgvWip = new System.Windows.Forms.DataGridView();
            this.DgvWipDet = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvWip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvWipDet)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1.Controls.Add(this.btnWips);
            this.splitContainer1.Panel1.Controls.Add(this.TxtWip);
            this.splitContainer1.Panel1.Controls.Add(this.lblWip);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1422, 586);
            this.splitContainer1.SplitterDistance = 72;
            this.splitContainer1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RdbTerminal);
            this.groupBox1.Controls.Add(this.RdbCable);
            this.groupBox1.Location = new System.Drawing.Point(522, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 47);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Desviar";
            // 
            // RdbTerminal
            // 
            this.RdbTerminal.AutoSize = true;
            this.RdbTerminal.Location = new System.Drawing.Point(6, 19);
            this.RdbTerminal.Name = "RdbTerminal";
            this.RdbTerminal.Size = new System.Drawing.Size(65, 17);
            this.RdbTerminal.TabIndex = 3;
            this.RdbTerminal.TabStop = true;
            this.RdbTerminal.Text = "Terminal";
            this.RdbTerminal.UseVisualStyleBackColor = true;
            // 
            // RdbCable
            // 
            this.RdbCable.AutoSize = true;
            this.RdbCable.Location = new System.Drawing.Point(111, 19);
            this.RdbCable.Name = "RdbCable";
            this.RdbCable.Size = new System.Drawing.Size(52, 17);
            this.RdbCable.TabIndex = 4;
            this.RdbCable.TabStop = true;
            this.RdbCable.Text = "Cable";
            this.RdbCable.UseVisualStyleBackColor = true;
            // 
            // btnWips
            // 
            this.btnWips.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWips.Location = new System.Drawing.Point(348, 26);
            this.btnWips.Name = "btnWips";
            this.btnWips.Size = new System.Drawing.Size(75, 28);
            this.btnWips.TabIndex = 1;
            this.btnWips.Text = "Buscar";
            this.btnWips.UseVisualStyleBackColor = true;
            this.btnWips.Click += new System.EventHandler(this.btnWips_Click);
            // 
            // TxtWip
            // 
            this.TxtWip.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWip.Location = new System.Drawing.Point(95, 28);
            this.TxtWip.Name = "TxtWip";
            this.TxtWip.Size = new System.Drawing.Size(235, 26);
            this.TxtWip.TabIndex = 2;
            this.TxtWip.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtWip_KeyDown);
            // 
            // lblWip
            // 
            this.lblWip.AutoSize = true;
            this.lblWip.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWip.Location = new System.Drawing.Point(45, 34);
            this.lblWip.Name = "lblWip";
            this.lblWip.Size = new System.Drawing.Size(44, 20);
            this.lblWip.TabIndex = 1;
            this.lblWip.Text = "Wip:";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.DgvWip);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.DgvWipDet);
            this.splitContainer2.Size = new System.Drawing.Size(1422, 510);
            this.splitContainer2.SplitterDistance = 68;
            this.splitContainer2.TabIndex = 0;
            // 
            // DgvWip
            // 
            this.DgvWip.AllowUserToAddRows = false;
            this.DgvWip.AllowUserToDeleteRows = false;
            this.DgvWip.AllowUserToResizeRows = false;
            this.DgvWip.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvWip.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DgvWip.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvWip.DefaultCellStyle = dataGridViewCellStyle2;
            this.DgvWip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvWip.Location = new System.Drawing.Point(0, 0);
            this.DgvWip.Name = "DgvWip";
            this.DgvWip.ReadOnly = true;
            this.DgvWip.RowHeadersVisible = false;
            this.DgvWip.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvWip.Size = new System.Drawing.Size(1422, 68);
            this.DgvWip.TabIndex = 0;
            this.DgvWip.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvWip_CellClick);
            // 
            // DgvWipDet
            // 
            this.DgvWipDet.AllowUserToAddRows = false;
            this.DgvWipDet.AllowUserToDeleteRows = false;
            this.DgvWipDet.AllowUserToResizeRows = false;
            this.DgvWipDet.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvWipDet.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DgvWipDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvWipDet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvWipDet.Location = new System.Drawing.Point(0, 0);
            this.DgvWipDet.Name = "DgvWipDet";
            this.DgvWipDet.ReadOnly = true;
            this.DgvWipDet.RowHeadersVisible = false;
            this.DgvWipDet.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvWipDet.Size = new System.Drawing.Size(1422, 438);
            this.DgvWipDet.TabIndex = 0;
            this.DgvWipDet.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvWipDet_CellDoubleClick);
            // 
            // frmDesviacionTerminales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1422, 586);
            this.Controls.Add(this.splitContainer1);
            this.Name = "frmDesviacionTerminales";
            this.Text = "Desviacion terminales";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvWip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvWipDet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label lblWip;
        private System.Windows.Forms.TextBox TxtWip;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Button btnWips;
        private System.Windows.Forms.DataGridView DgvWip;
        private System.Windows.Forms.DataGridView DgvWipDet;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton RdbTerminal;
        private System.Windows.Forms.RadioButton RdbCable;
    }
}

