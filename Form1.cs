﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ModDesviacionTerminales
{
    public partial class frmDesviacionTerminales : Form
    {
        public frmDesviacionTerminales()
        {
            InitializeComponent();
            this.ActiveControl = TxtWip;
            this.WindowState = FormWindowState.Maximized;
        }

        private DataTable Findwip(string wip)
        {
            string queryBuscarWip = $"SELECT Status,KindOfAU, wSort, WIP, Rev, PN, BalanceProcess, BalanceSubStorage, BalanceAssy, BalancePack, BalanceShipped, DueDateCustomer, ProcFDispMat, CutFProm FROM tblwip WHERE wip Like '%{wip}' AND status = 'Open'";
            DataTable tblwip = new DataTable();

            Connection Conexion = new Connection();
            string connectionString = Conexion.ConnectionData();
            SqlConnection cnn;
           
            cnn = new SqlConnection(connectionString);
            cnn.Open();
            try
            {
                if (cnn != null && cnn.State == ConnectionState.Open)
                {
                    SqlDataAdapter myAdapter = new SqlDataAdapter();
                    SqlCommand command = new SqlCommand(queryBuscarWip, cnn);
                    SqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection);
                    tblwip.Load(dr);
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                cnn.Close();
            }
            return tblwip;
        }

        private void LoadDgvWip(DataTable Tblwip)
        {
            DgvWip.DataSource = null;
            DgvWip.Rows.Clear();


            if (Tblwip.Rows.Count > 0)
            {
                DgvWip.DataSource = (Tblwip);
                DgvWip.AutoResizeColumns();
                DgvWip.CurrentCell = null;
            }
            else
                MessageBox.Show("No existe ningun wip con la terminacion indicada.");
        }

        private DataTable LoadWipDet(string wip)
        {
            //string queryBuscarWip = $"SELECT WIP, CWO, AU, Rev, WID, Wire, LengthWire, TermA, TermB, TABalance, TBBalance, PWOA, PWOB, MaqUsed  FROM tblwipdet where WIP = '{wip}'";
            string queryBuscarWip = $"SELECT Wip, AU,PN, Qty,Balance, MaterialGroup from tblbomwip where wip = @wip and MaterialGroup = 'MG01'";
            DataTable tblwipDet = new DataTable();

            Connection Conexion = new Connection();
            string connectionString = Conexion.ConnectionData();
            SqlConnection cnn;
            cnn = new SqlConnection(connectionString);

            try
            {
                cnn.Open();
                if (cnn != null && cnn.State == ConnectionState.Open)
                {
                    SqlDataAdapter myAdapter = new SqlDataAdapter();
                    SqlCommand command = new SqlCommand(queryBuscarWip, cnn);
                    command.Parameters.Add("@wip", SqlDbType.NVarChar).Value = wip;
                    SqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection);
                    tblwipDet.Load(dr);   
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                cnn.Close();
            }
            return tblwipDet;
        }
        private void LoadDgvWipdet(DataTable wipDet)
        {
            DgvWipDet.DataSource = null;
            DgvWipDet.Rows.Clear();

            if(wipDet.Rows.Count > 0)
            {
                DgvWipDet.DataSource = wipDet;
                DgvWipDet.AutoResizeColumns();
                DgvWipDet.CurrentCell = null;
            }
        }
        private char firstchar(string splitString)
        {
            char fchar = splitString[0];
            return fchar;
        }
        private void btnWips_Click(object sender, EventArgs e)
        {
            DataTable wipTable = new DataTable();
            wipTable = Findwip(TxtWip.Text);
            LoadDgvWip(wipTable);
        }

        private void TxtWip_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                DataTable wipTable = new DataTable();
                wipTable = Findwip(TxtWip.Text);
                LoadDgvWip(wipTable);
            }
        }
        private void DgvWip_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (DgvWip.SelectedCells.Count > 0)
            {
                if(e.RowIndex >= 0)
                {
                    int Rowindex = DgvWip.SelectedCells[0].RowIndex;
                    DataGridViewRow selectedRow = DgvWip.Rows[Rowindex];
                    string wip = Convert.ToString(selectedRow.Cells["WIP"].Value);
                    DataTable WipDet = new DataTable();
                    WipDet = LoadWipDet(wip);
                    LoadDgvWipdet(WipDet);
                }       
            }
        }

        private void DgvWipDet_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if(DgvWipDet.SelectedRows.Count > 0)
            {
                if(e.RowIndex >= 0)
                {
                    int rowindex = DgvWipDet.SelectedRows[0].Index;
                    DataGridViewRow selectedRow = DgvWipDet.Rows[rowindex];
                    string PartNumber = Convert.ToString(selectedRow.Cells["PN"].Value);
                    string wip = Convert.ToString(selectedRow.Cells["WIP"].Value);
                    char FChar = firstchar(PartNumber);
                    if (RdbTerminal.Checked)
                    {
                        if (FChar == 'T' || FChar == 'G')
                        {
                            FrmDesviar desviacion = new FrmDesviar("Terminal", PartNumber, wip);
                            desviacion.Show();
                        }
                        else
                            MessageBox.Show("Este componente no es una terminal.");
                    }

                    else if (RdbCable.Checked)
                    {
                        if (FChar == 'W')
                        {
                            FrmDesviar desviacion = new FrmDesviar("Wire", PartNumber, wip);
                            desviacion.Show();
                        }

                        else
                            MessageBox.Show("Este componente no es un cable");
                    }
                    else
                        MessageBox.Show("Selecciona que tipo de material quieres desviar cable o terminal");
                }
            }
           
        }
    }
}
