﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ModDesviacionTerminales
{
    public partial class FrmDesviar : Form
    {
        public FrmDesviar(string tipoDesviacion, string NumeroParte, string wip)
        {
            InitializeComponent();
            LblPartNumberText.Text = NumeroParte;
            LblWipText.Text = wip;
            lblTipoDesviacionText.Text = tipoDesviacion;
            LoadAlternTerm(NumeroParte);


        }
        private DataTable LoadWipDet(string wip, string term)
        {
            string queryBuscarWip = $"SELECT * FROM Tblwipdet WHERE WIP=@WIP AND (TermA=@Term OR TermB=@Term)";
            DataTable tblwipDet = new DataTable();

            Connection Conexion = new Connection();
            string connectionString = Conexion.ConnectionData();
            SqlConnection cnn;
            cnn = new SqlConnection(connectionString);

            try
            {
                cnn.Open();
                if (cnn != null && cnn.State == ConnectionState.Open)
                {
                    SqlDataAdapter myAdapter = new SqlDataAdapter();
                    SqlCommand command = new SqlCommand(queryBuscarWip, cnn);
                    command.Parameters.Add("@WIP", SqlDbType.NVarChar).Value = wip;
                    command.Parameters.Add("@Term", SqlDbType.NVarChar).Value = term;
                    SqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection);
                    tblwipDet.Load(dr);
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                cnn.Close();
            }
            return tblwipDet;
        }

        private DataTable LoadDesviacionesTerm(string term, string newTerm)
        {
            string queryBuscarWip = $"SELECT * FROM tblDesviacionesTerm where PnOriginal = @term and PnValido = @newTerm";
            DataTable tblDesviacionesTerm = new DataTable();

            Connection Conexion = new Connection();
            string connectionString = Conexion.ConnectionData();
            SqlConnection cnn;
            cnn = new SqlConnection(connectionString);

            try
            {
                cnn.Open();
                if (cnn != null && cnn.State == ConnectionState.Open)
                {
                    SqlDataAdapter myAdapter = new SqlDataAdapter();
                    SqlCommand command = new SqlCommand(queryBuscarWip, cnn);
                    command.Parameters.Add("@Newterm", SqlDbType.NVarChar).Value = newTerm;
                    command.Parameters.Add("@term", SqlDbType.NVarChar).Value = term;
                    SqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection);
                    tblDesviacionesTerm.Load(dr);
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                cnn.Close();
            }
            return tblDesviacionesTerm;
        }

        private void LoadAlternTerm(string term)
        {
            string queryBuscarWip = $"SELECT DISTINCT PnValido FROM tblDesviacionesTerm where PnOriginal = @term";
            DataTable tblDesviacionesTerm = new DataTable();

            Connection Conexion = new Connection();
            string connectionString = Conexion.ConnectionData();
            SqlConnection cnn;
            cnn = new SqlConnection(connectionString);

            try
            {
                cnn.Open();
                if (cnn != null && cnn.State == ConnectionState.Open)
                {
                    SqlDataAdapter myAdapter = new SqlDataAdapter();
                    SqlCommand command = new SqlCommand(queryBuscarWip, cnn);
                    command.Parameters.Add("@term", SqlDbType.NVarChar).Value = term;
                    SqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection);
                    tblDesviacionesTerm.Load(dr);
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                cnn.Close();
            }

            cmbNewTerm.Items.Clear();
            if(tblDesviacionesTerm.Rows.Count > 0)
            {
                for(int i = 0; i < tblDesviacionesTerm.Rows.Count; i++)
                {
                    string LoadData = tblDesviacionesTerm.Rows[i]["PnValido"].ToString();
                    cmbNewTerm.Items.Add(LoadData);
                }
            }
            
        }

        private string GetPnDescripton(string PN)
        {
            string queryBuscarWip = $"SELECT TOP(1) Description FROM tblItemsQB WHERE PN = @PN";
            DataTable Description = new DataTable();

            Connection Conexion = new Connection();
            string connectionString = Conexion.ConnectionData();
            SqlConnection cnn;
            cnn = new SqlConnection(connectionString);

            try
            {
                cnn.Open();
                if (cnn != null && cnn.State == ConnectionState.Open)
                {
                    SqlDataAdapter myAdapter = new SqlDataAdapter();
                    SqlCommand command = new SqlCommand(queryBuscarWip, cnn);
                    command.Parameters.Add("@PN", SqlDbType.NVarChar).Value = PN;
                    SqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection);
                    Description.Load(dr);
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                cnn.Close();
            }

            string pn = "";
            if (Description.Rows.Count > 0)
            {
                pn = Description.Rows[0]["Description"].ToString();
            }
            return pn;
        }
        private void UpdateTblwipdet(string wip, string ActualTerm, string newTerm, string side)
        {
            string queryBuscarWip = $"UPDATE tblWipDet SET Term{side}=@NewTerm WHERE WIP=@WIP AND Term{side}=@ActualTerm";
            DataTable Description = new DataTable();
            Connection Conexion = new Connection();
            string connectionString = Conexion.ConnectionData();
            SqlConnection cnn;
            cnn = new SqlConnection(connectionString);

            try
            {
                cnn.Open();
                if (cnn != null && cnn.State == ConnectionState.Open)
                {
                    SqlCommand command = new SqlCommand(queryBuscarWip, cnn);
                    command.Parameters.Add("@ActualTerm", SqlDbType.NVarChar).Value = ActualTerm;
                    command.Parameters.Add("@WIP", SqlDbType.NVarChar).Value = wip;
                    command.Parameters.Add("@NewTerm", SqlDbType.NVarChar).Value = newTerm;
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                cnn.Close();
            }
        }
        private void UpdateBoms(string wip, string ActualTerm, string newTerm, string description, string table)
        {
            //update tblBomwip, BomCWO Or BomPWO 
            // table variable is the key 
            string queryBuscarWip = $"UPDATE {table} SET PN=@Newterm, Description=@Descrip WHERE WIP=@WIP AND PN=@ActualTerm";
            DataTable Description = new DataTable();

            Connection Conexion = new Connection();
            string connectionString = Conexion.ConnectionData();
            SqlConnection cnn;
            cnn = new SqlConnection(connectionString);

            try
            {
                cnn.Open();
                if (cnn != null && cnn.State == ConnectionState.Open)
                {
                    SqlCommand command = new SqlCommand(queryBuscarWip, cnn);
                    command.Parameters.Add("@ActualTerm", SqlDbType.NVarChar).Value = ActualTerm;
                    command.Parameters.Add("@WIP", SqlDbType.NVarChar).Value = wip;
                    command.Parameters.Add("@NewTerm", SqlDbType.NVarChar).Value = newTerm;
                    command.Parameters.Add("@Descrip", SqlDbType.NVarChar).Value = description;
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                cnn.Close();
            }
        }

        private void UpdateWipCord(string wip, string ActualTerm, string newTerm)
        {
            string queryBuscarWip = $"UPDATE tblWipCord SET PN=@NewTerm WHERE WIP=@WIP AND PN=@ActualTerm";
            DataTable Description = new DataTable();
            Connection Conexion = new Connection();
            string connectionString = Conexion.ConnectionData();
            SqlConnection cnn;
            cnn = new SqlConnection(connectionString);

            try
            {
                cnn.Open();
                if (cnn != null && cnn.State == ConnectionState.Open)
                {
                    // myAdapter = new SqlDataAdapter();
                    SqlCommand command = new SqlCommand(queryBuscarWip, cnn);
                    command.Parameters.Add("@ActualTerm", SqlDbType.NVarChar).Value = ActualTerm;
                    command.Parameters.Add("@WIP", SqlDbType.NVarChar).Value = wip;
                    command.Parameters.Add("@NewTerm", SqlDbType.NVarChar).Value = newTerm;
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                cnn.Close();
            }
        }

        private bool checkIfTerminalexists(string terminal)
        {
            string queryBuscarWip = $"SELECT PN FROM tblItemsQB WHERE PN=@PN";
            DataTable tblItemsQb = new DataTable();

            Connection Conexion = new Connection();
            string connectionString = Conexion.ConnectionData();
            SqlConnection cnn;
            cnn = new SqlConnection(connectionString);

            try
            {
                cnn.Open();
                if (cnn != null && cnn.State == ConnectionState.Open)
                {
                    SqlDataAdapter myAdapter = new SqlDataAdapter();
                    SqlCommand command = new SqlCommand(queryBuscarWip, cnn);
                    command.Parameters.Add("@PN", SqlDbType.NVarChar).Value = terminal;
                    SqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection);
                    tblItemsQb.Load(dr);
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                cnn.Close();
            }
            return tblItemsQb.Rows.Count > 0;
        }

        private DataTable loadtermSpecs(string newTerminal)
        {
            string queryBuscarWip = $"SELECT * FROM tblTermSpecs WHERE PN=@PN";
            DataTable tblTermSpecs = new DataTable();

            Connection Conexion = new Connection();
            string connectionString = Conexion.ConnectionData();
            SqlConnection cnn;
            cnn = new SqlConnection(connectionString);

            try
            {
                cnn.Open();
                if (cnn != null && cnn.State == ConnectionState.Open)
                {
                    SqlDataAdapter myAdapter = new SqlDataAdapter();
                    SqlCommand command = new SqlCommand(queryBuscarWip, cnn);
                    command.Parameters.Add("@PN", SqlDbType.NVarChar).Value = newTerminal;
                    SqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection);
                    tblTermSpecs.Load(dr);
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                cnn.Close();
            }
            return tblTermSpecs;
        }
        private void processTerminal()
        {
            bool terminalExist = checkIfTerminalexists(cmbNewTerm.Text.Trim());

            if (terminalExist && cmbNewTerm.Text.Trim() != "")
            {
                DataTable tblwipdet = new DataTable();
                DataTable tbltermSpecs = new DataTable();
                DataTable TblDesviacionesTerm = new DataTable();
                string wip = LblWipText.Text.Trim();
                string actualterm = LblPartNumberText.Text.Trim();
                string newTerm = cmbNewTerm.Text.Trim();
                DateTime thisDay = DateTime.Today;
                string getDescription = GetPnDescripton(newTerm);
                tbltermSpecs = loadtermSpecs(newTerm);
                tblwipdet = LoadWipDet(wip, actualterm);
                TblDesviacionesTerm = LoadDesviacionesTerm(actualterm, newTerm);

                
                if(TblDesviacionesTerm.Rows.Count > 0)
                {
                    for (int i = 0; tblwipdet.Rows.Count - 1 >= i; i++)
                    {
                        string getWire = tblwipdet.Rows[i]["Wire"].ToString();
                        string onlypartNumber = getWire.Substring(0, 5);
                        int countDesviacinesTerm = TblDesviacionesTerm.AsEnumerable().Count(row => row.Field<string>("Pnoriginal") == actualterm && (row.Field<string>("Pnvalido") == newTerm) && row.Field<string>("AwgValido") == onlypartNumber && row.Field<string>("Notas") == "VALIDADA");
                        if (countDesviacinesTerm > 0)
                        {
                            int count = tbltermSpecs.AsEnumerable().Count(row => row.Field<string>("WTAWG") == onlypartNumber && (row.Field<bool>("PTA") == true || (row.Field<DateTime>("TemporalApprovedDate") >= thisDay  && row.Field<bool>("TemporalApproved") == true)) && row.Field<string>("PN") == newTerm);
                            if (count == 0)
                            {
                                MessageBox.Show($"El Cable {onlypartNumber} no esta validado para la Terminal {newTerm} contacte al area de aplicadores e ingenieria");
                                return;
                            }
                            UpdateTblwipdet(wip, actualterm, newTerm, "A");
                            UpdateTblwipdet(wip, actualterm, newTerm, "B");
                            UpdateBoms(wip, actualterm, newTerm, getDescription, "tblBOMWIP");

                            //Toma los Valores unicos del CWO y despues los actualiza
                            var CWO = (from row in tblwipdet.AsEnumerable() select row["CWO"]).Distinct().ToList();
                            foreach (var c in CWO)
                                UpdateBoms(wip, actualterm, newTerm, getDescription, "tblBOMCWO");

                            //toma los valores unicos de PWO y los actualiza en el PWOBOM
                            var PWO = (from row in tblwipdet.AsEnumerable() select (row["PWOA"], row["PWOB"])).Distinct().ToList();
                            foreach (var p in PWO)
                                UpdateBoms(wip, actualterm, newTerm, getDescription, "tblBOMPWO");

                            UpdateWipCord(wip, actualterm, newTerm);
                            MessageBox.Show("Se a cambiado la terminal exitosamente");
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show($"este cable {onlypartNumber} no esta dado de alta para esta terminal en la tabla de desviaciones favor de contactar al area de apliacdores e ingenieria.");
                            break;
                        }
                    }
                }
                else
                    MessageBox.Show("El numero de parte indicado no existe");
            }
        }
        private void processWire()
        {

        }
        private void BtnDesviar_Click(object sender, EventArgs e)
        {
            if (cmbNewTerm.Text.Trim() != "" && lblTipoDesviacionText.Text == "Terminal")
                processTerminal();

            if (cmbNewTerm.Text.Trim() != "" && lblTipoDesviacionText.Text == "Wire")
                processWire();
        }
    }
}