﻿
namespace ModDesviacionTerminales
{
    partial class FrmDesviar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPn = new System.Windows.Forms.Label();
            this.LblPartNumberText = new System.Windows.Forms.Label();
            this.lblWip = new System.Windows.Forms.Label();
            this.LblWipText = new System.Windows.Forms.Label();
            this.lblTipoDesviacionText = new System.Windows.Forms.Label();
            this.LblTipoDesviacion = new System.Windows.Forms.Label();
            this.BtnDesviar = new System.Windows.Forms.Button();
            this.cmbNewTerm = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblPn
            // 
            this.lblPn.AutoSize = true;
            this.lblPn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPn.Location = new System.Drawing.Point(502, 32);
            this.lblPn.Name = "lblPn";
            this.lblPn.Size = new System.Drawing.Size(50, 25);
            this.lblPn.TabIndex = 0;
            this.lblPn.Text = "PN:";
            // 
            // LblPartNumberText
            // 
            this.LblPartNumberText.AutoSize = true;
            this.LblPartNumberText.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPartNumberText.Location = new System.Drawing.Point(558, 32);
            this.LblPartNumberText.Name = "LblPartNumberText";
            this.LblPartNumberText.Size = new System.Drawing.Size(25, 25);
            this.LblPartNumberText.TabIndex = 1;
            this.LblPartNumberText.Text = "0";
            // 
            // lblWip
            // 
            this.lblWip.AutoSize = true;
            this.lblWip.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWip.Location = new System.Drawing.Point(90, 32);
            this.lblWip.Name = "lblWip";
            this.lblWip.Size = new System.Drawing.Size(61, 25);
            this.lblWip.TabIndex = 4;
            this.lblWip.Text = "WIP:";
            // 
            // LblWipText
            // 
            this.LblWipText.AutoSize = true;
            this.LblWipText.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblWipText.Location = new System.Drawing.Point(146, 32);
            this.LblWipText.Name = "LblWipText";
            this.LblWipText.Size = new System.Drawing.Size(25, 25);
            this.LblWipText.TabIndex = 5;
            this.LblWipText.Text = "0";
            // 
            // lblTipoDesviacionText
            // 
            this.lblTipoDesviacionText.AutoSize = true;
            this.lblTipoDesviacionText.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoDesviacionText.Location = new System.Drawing.Point(357, 141);
            this.lblTipoDesviacionText.Name = "lblTipoDesviacionText";
            this.lblTipoDesviacionText.Size = new System.Drawing.Size(25, 25);
            this.lblTipoDesviacionText.TabIndex = 6;
            this.lblTipoDesviacionText.Text = "0";
            // 
            // LblTipoDesviacion
            // 
            this.LblTipoDesviacion.AutoSize = true;
            this.LblTipoDesviacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTipoDesviacion.Location = new System.Drawing.Point(90, 141);
            this.LblTipoDesviacion.Name = "LblTipoDesviacion";
            this.LblTipoDesviacion.Size = new System.Drawing.Size(188, 25);
            this.LblTipoDesviacion.TabIndex = 7;
            this.LblTipoDesviacion.Text = "Tipo Desviacion:";
            // 
            // BtnDesviar
            // 
            this.BtnDesviar.BackColor = System.Drawing.Color.Crimson;
            this.BtnDesviar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDesviar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BtnDesviar.Location = new System.Drawing.Point(350, 208);
            this.BtnDesviar.Name = "BtnDesviar";
            this.BtnDesviar.Size = new System.Drawing.Size(168, 45);
            this.BtnDesviar.TabIndex = 8;
            this.BtnDesviar.Text = "Desviar";
            this.BtnDesviar.UseVisualStyleBackColor = false;
            this.BtnDesviar.Click += new System.EventHandler(this.BtnDesviar_Click);
            // 
            // cmbNewTerm
            // 
            this.cmbNewTerm.FormattingEnabled = true;
            this.cmbNewTerm.Location = new System.Drawing.Point(95, 75);
            this.cmbNewTerm.Name = "cmbNewTerm";
            this.cmbNewTerm.Size = new System.Drawing.Size(617, 21);
            this.cmbNewTerm.TabIndex = 9;
            // 
            // FrmDesviar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 344);
            this.Controls.Add(this.cmbNewTerm);
            this.Controls.Add(this.BtnDesviar);
            this.Controls.Add(this.LblTipoDesviacion);
            this.Controls.Add(this.lblTipoDesviacionText);
            this.Controls.Add(this.LblWipText);
            this.Controls.Add(this.lblWip);
            this.Controls.Add(this.LblPartNumberText);
            this.Controls.Add(this.lblPn);
            this.MaximizeBox = false;
            this.Name = "FrmDesviar";
            this.Text = "FrmDesviar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPn;
        private System.Windows.Forms.Label LblPartNumberText;
        private System.Windows.Forms.Label lblWip;
        private System.Windows.Forms.Label LblWipText;
        private System.Windows.Forms.Label lblTipoDesviacionText;
        private System.Windows.Forms.Label LblTipoDesviacion;
        private System.Windows.Forms.Button BtnDesviar;
        private System.Windows.Forms.ComboBox cmbNewTerm;
    }
}